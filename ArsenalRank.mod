<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="Arsenal Rank" version="1.0.0" date="06/22/2009" >
        
        <Author name="Werit" email="weritblog@gmail.com" />
        <Description text="Notifies you of fights in every tier." />
        
        <Dependencies>
	    <Dependency name="EA_ChatWindow"/>
        </Dependencies>
        
        <Files>
            <File name="ArsenalRank.lua" />
        </Files>
        
        <OnInitialize>
            <CallFunction name="ArsenalRank.init" />
        </OnInitialize>
        <OnUpdate/>
        <OnShutdown/>
    </UiMod>
</ModuleFile>
